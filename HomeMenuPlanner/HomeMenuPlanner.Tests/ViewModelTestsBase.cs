﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Moq;
using Pietkiewicz.pl.HomeMenuPlanner.Models;
using Pietkiewicz.pl.HomeMenuPlanner.Services;

namespace HomeMenuPlanner.Tests
{
    public class ViewModelTestsBase
    {
        protected Mock<IEventAggregator> EventAggregatorMock;
        protected Mock<IDialogService> DialogServiceMock;
        protected Mock<IActivate> ActivateMock;
        protected Mock<ISettingsService> SettingsServiceMock;

        public ViewModelTestsBase() : base()
        {
            EventAggregatorMock = new Mock<IEventAggregator>();
            DialogServiceMock = new Mock<IDialogService>();
            SettingsServiceMock = new Mock<ISettingsService>();
            ActivateMock = new Mock<IActivate>();

            SettingsServiceMock.Setup(x => x.GetInt(Setting.Keys.BudgetStartDate, It.IsAny<int>())).Returns(10);
        }
    }
}
