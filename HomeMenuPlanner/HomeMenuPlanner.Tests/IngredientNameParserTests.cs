﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pietkiewicz.pl.HomeMenuPlanner.Models;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace HomeMenuPlanner.Tests
{
    [TestClass]
    public class IngredientNameParserTests
    {

        public IngredientNameParserTests()
        {
        }

        [TestMethod]
        public void CanParseValidIngredientText()
        {
            var result = IngredientVm.Parse("1kg cukru");

            Assert.IsNotNull(result);           
        }

        [TestMethod]
        public void IngredientParserParsesNameCorrectly1()
        {
            var result = IngredientVm.Parse("1kg cukru");

            Assert.AreEqual("cukru", result.Name);
        }

        [TestMethod]
        public void IngredientParserParsesUnitKeyCorrectly1()
        {
            var result = IngredientVm.Parse("1kg cukru");

            Assert.AreEqual("kg", result.UnitKey);
        }

        [TestMethod]
        public void IngredientParserParsesValueCorrectly1()
        {
            var result = IngredientVm.Parse("1kg cukru");

            Assert.AreEqual("1", result.Value);
        }

        [TestMethod]
        public void IngredientParserParsesNameCorrectly2()
        {
            var result = IngredientVm.Parse("0.5 kg cukru");

            Assert.AreEqual("cukru", result.Name);
        }

        [TestMethod]
        public void IngredientParserParsesUnitKeyCorrectly2()
        {
            var result = IngredientVm.Parse("0.5 kg cukru");

            Assert.AreEqual("kg", result.UnitKey);
        }

        [TestMethod]
        public void IngredientParserParsesValueCorrectly2()
        {
            var result = IngredientVm.Parse("0.5 kg cukru");

            Assert.AreEqual("0.5", result.Value);
        }

        [TestMethod]
        public void IngredientParserParsesNameCorrectly3()
        {
            var result = IngredientVm.Parse("1/2 łyżeczki soli");

            Assert.AreEqual("soli", result.Name);
        }

        [TestMethod]
        public void IngredientParserParsesUnitKeyCorrectly3()
        {
            var result = IngredientVm.Parse("1/2 łyżeczki soli");

            Assert.AreEqual("łyżeczki", result.UnitKey);
        }

        [TestMethod]
        public void IngredientParserParsesValueCorrectly3()
        {
            var result = IngredientVm.Parse("1/2 łyżeczki soli");

            Assert.AreEqual("1/2", result.Value);
        }

        [TestMethod]
        public void IngredientParserParsesNameCorrectly4()
        {
            var result = IngredientVm.Parse("pektyna jabłkowa (ilość pektyny wg opisu na opakowaniu)");

            Assert.AreEqual("pektyna jabłkowa (ilość pektyny wg opisu na opakowaniu)", result.Name);
        }

        [TestMethod]
        public void IngredientParserParsesUnitKeyCorrectly4()
        {
            var result = IngredientVm.Parse("pektyna jabłkowa (ilość pektyny wg opisu na opakowaniu)");

            Assert.AreEqual("", result.UnitKey);
        }

        [TestMethod]
        public void IngredientParserParsesValueCorrectly4()
        {
            var result = IngredientVm.Parse("pektyna jabłkowa (ilość pektyny wg opisu na opakowaniu)");

            Assert.AreEqual("", result.Value);
        }

        [TestMethod]
        public void IngredientParserParsesNameCorrectly5()
        {
            var result = IngredientVm.Parse("100-150 g rukoli, (1 opakowanie)");

            Assert.AreEqual("rukoli, (1 opakowanie)", result.Name);
        }

        [TestMethod]
        public void IngredientParserParsesUnitKeyCorrectly5()
        {
            var result = IngredientVm.Parse("100-150 g rukoli, (1 opakowanie)");

            Assert.AreEqual("g", result.UnitKey);
        }

        [TestMethod]
        public void IngredientParserParsesValueCorrectly5()
        {
            var result = IngredientVm.Parse("100-150 g rukoli, (1 opakowanie)");

            Assert.AreEqual("100-150", result.Value);
        }

    }
}
