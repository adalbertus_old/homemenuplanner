﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Pietkiewicz.pl.HomeMenuPlanner.Common.Converters
{
    [ValueConversion(typeof(object), typeof(string))]
    public class StringFormatConverter : IValueConverter
    {
        public string StringFormat { get; set; }

        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty(StringFormat))
            {
                return string.Empty;
            }
            return string.Format(StringFormat, value);
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
