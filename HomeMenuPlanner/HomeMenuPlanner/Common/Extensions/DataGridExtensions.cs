﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace Pietkiewicz.pl.HomeMenuPlanner.Common.Extensions
{
    public class DataGridExtensions : DependencyObject
    {


        public static IList GetSelectedItems(DependencyObject obj)
        {
            return (IList)obj.GetValue(SelectedItemsProperty);
        }

        public static void SetSelectedItems(DependencyObject obj, IList value)
        {
            obj.SetValue(SelectedItemsProperty, value);
        }

        // Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.RegisterAttached("SelectedItems", typeof(IList), typeof(DataGridExtensions), new FrameworkPropertyMetadata(default(IList), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnPropertyChangedCallback));
        //DependencyProperty.RegisterAttached("SelectedItems", typeof(IList), typeof(DataGridExtensions), new PropertyMetadata(default(IList), OnPropertyChangedCallback));

        private static void OnPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var dataGrid = dependencyObject as DataGrid;
            if (dataGrid == null)
            {
                return;
            }
            dataGrid.SelectionChanged += (sender, args) =>
            {
                SetSelectedItems(dependencyObject, dataGrid.SelectedItems);
            };
        }
    }
}
