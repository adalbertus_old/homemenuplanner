﻿using System;

namespace Pietkiewicz.pl.HomeMenuPlanner.Common.Extensions
{
    public static class ValidationsExtensions
    {
        public static void ThrowIfNull<T>(this T obj, string parameterName)
            where T : class
        {
            if (obj == null) throw new ArgumentNullException(parameterName);
        }

        public static void VerifyNotNull<T>(this T? obj, string parameterName)
            where T : struct
        {
            if (obj == null)
            {
                throw new ArgumentNullException(parameterName);
            }
        }

        public static void ThrowIfNull(this string text, string parameterName)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                throw new ArgumentNullException(parameterName);
            }
        }
    }
}