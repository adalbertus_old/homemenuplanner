﻿namespace Pietkiewicz.pl.HomeMenuPlanner.Common
{
    public enum CommandType
    {
        CloseSettingsFlyout,
        RecepieSelected,
        OpenEditRecipie,
        CloseEditRecipie,
        NewIngredientNameChanged
    }

    public class SimpleCommand
    {
        public CommandType CommandType { get; set; }
        public object Data { get; set; }
   }
}