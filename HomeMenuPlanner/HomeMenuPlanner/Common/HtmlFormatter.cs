﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using Xceed.Wpf.Toolkit;

namespace Pietkiewicz.pl.HomeMenuPlanner.Common
{
    public class HtmlFormatter : ITextFormatter
    {
        public string GetText(FlowDocument document)
        {
            var textRange = new TextRange(document.ContentStart, document.ContentEnd);
            var text = textRange.Text;
            return text;
        }

        public void SetText(FlowDocument document, string text)
        {
            new TextRange(document.ContentStart, document.ContentEnd).Text = text;
        }
    }
}
