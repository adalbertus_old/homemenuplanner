using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using Caliburn.Micro;
using Castle.Core;
using Castle.Core.Internal;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using MahApps.Metro.Controls.Dialogs;
using Pietkiewicz.pl.HomeMenuPlanner.Common.Keyboard;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels;

namespace Pietkiewicz.pl.HomeMenuPlanner
{
    public class AppBootstraper : BootstrapperBase
    {
        public AppBootstraper()
        {
            Initialize();
        }

        public static IWindsorContainer Container { get; private set; }

        protected override void Configure()
        {
            Container = new WindsorContainer().Register(
                Component.For<IWindowManager>().ImplementedBy<WindowManager>().LifeStyle.Is(LifestyleType.Singleton),
                Component.For<IEventAggregator>().ImplementedBy<EventAggregator>().LifeStyle.Is(LifestyleType.Singleton),
                Classes.FromThisAssembly().BasedOn(typeof (BaseMetroDialog)).WithServiceSelf().LifestyleTransient(),
                Classes.FromThisAssembly().BasedOn(typeof(ViewModelBase)).WithServiceSelf().LifestyleTransient(),
                Classes.FromThisAssembly().BasedOn(typeof(ShellViewModel)).WithServiceSelf().LifestyleTransient(),
                Classes.FromAssemblyContaining<global::Pietkiewicz.pl.HomeMenuPlanner.App>()
                    .Pick()
                    .WithService.DefaultInterfaces().LifestyleSingleton()
                );


            //cal:Message.Attach="[Shortcut Escape] = [Action Cancel]"
            var currentParser = Parser.CreateTrigger;
            Parser.CreateTrigger = (target, triggerText) => ShortcutParser.CanParse(triggerText)
                                                                ? ShortcutParser.CreateTrigger(triggerText)
                                                                : currentParser(target, triggerText);
            // other key short cuts solution: http://stackoverflow.com/a/16731847
        }

        protected override object GetInstance(Type service, string key)
        {
            object result = null;
            if (string.IsNullOrWhiteSpace(key))
            {
                result = Container.Resolve(service);
            }
            else
            {
                result = Container.Resolve(key, service);
            }

            return result;
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            return new[]
            {
                Assembly.GetExecutingAssembly()
            };
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            var result = (IEnumerable<object>) Container.ResolveAll(service);
            return result;
        }

        protected override void BuildUp(object instance)
        {
            Container.GetType().GetProperties()
                .Where(property => property.CanWrite && property.PropertyType.IsPublic)
                .Where(property => Container.Kernel.HasComponent(property.PropertyType))
                .ForEach(property => property.SetValue(instance, Container.Resolve(property.PropertyType), null)
                );
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }
    }
}