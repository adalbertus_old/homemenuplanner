﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Syncfusion.Windows.Controls.RichTextBoxAdv;

namespace Pietkiewicz.pl.HomeMenuPlanner.Controls
{
    public class RichTextBox : SfRichTextBoxAdv
    {
        public bool IgnoreHtmlUpdate { get; set; }
        public string HtmlText
        {
            get { return (string)GetValue(HtmlTextProperty); }
            set { SetValue(HtmlTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HtmlText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HtmlTextProperty = DependencyProperty.Register("HtmlText", typeof(string), typeof(RichTextBox), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnHtmlTextPropertyChanged));

        private static void OnHtmlTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var richTextBox = d as RichTextBox;
            if (richTextBox == null)
            {
                return;
            }
            if (richTextBox.IgnoreHtmlUpdate)
            {
                return;
            }
            if (e.NewValue == null)
            {
                //d.SetValue(HtmlTextProperty, string.Empty);
            }
            else
            {
                var html = e.NewValue.ToString();
                using (var memoryStream = new MemoryStream())
                {
                    using (var writer = new StreamWriter(memoryStream))
                    {
                        writer.Write(html);
                        writer.Flush();
                        memoryStream.Position = 0;
                        richTextBox.Load(memoryStream, FormatType.Html);
                        memoryStream.Flush();
                    }
                }
            }
        }

        public RichTextBox()
        {            
            ContentChanged += OnContentChanged;

            CommandBinding pasteCommandBinding = CommandBindings.Cast<CommandBinding>().FirstOrDefault(binding => binding.Command == PasteCommand);
            if (pasteCommandBinding != null)
            {
                CommandBindings.Remove(pasteCommandBinding);
            }

            this.CommandBindings.Add(new CommandBinding((ICommand)SfRichTextBoxAdv.PasteCommand, new ExecutedRoutedEventHandler(OnPasteExecuted), new CanExecuteRoutedEventHandler(this.OnPasteCanExecute)));
        }

        private void OnPasteCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Clipboard.ContainsImage() || Clipboard.ContainsText() || Clipboard.ContainsData(DataFormats.Rtf) || Clipboard.ContainsData(DataFormats.Html);
        }

        private void OnPasteExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            HtmlText = Clipboard.GetText();
            this.Focus();
        }

        private void OnPasteCommand(object sender, DataObjectPastingEventArgs dataObjectPastingEventArgs)
        {
            
        }

        private void OnContentChanged(object o, ContentChangedEventArgs args)
        {
            //    var html = string.Empty;
            using (var stream = new MemoryStream())
            {
                Save(stream, FormatType.Html);

                stream.Position = 0;
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    IgnoreHtmlUpdate = true;
                    HtmlText = reader.ReadToEnd();
                    IgnoreHtmlUpdate = false;
                }
            }
        }       
    }
}
