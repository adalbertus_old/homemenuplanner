﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Common;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models
{
    public class RecepieListItemVm : PropertyChangedBase
    {
        private readonly IEventAggregator _eventAggregator;

        public int RecepieId { get; set; }
        public string Name { get; set; }
        
        public IList<RecepieListItemVm> Items { get; set; }
        public RecepieListItemVm Parent { get; set; }

        public bool IsParent { get { return Parent == null; } }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                NotifyOfPropertyChange(() => IsSelected);
                PublishSelectionChanged();
            }
        }

        private void PublishSelectionChanged()
        {
            if (!IsNotifying)
            {
                return;
            }
            _eventAggregator.PublishOnUIThread(new SimpleCommand
            {
                CommandType = CommandType.RecepieSelected,
                Data = this
            });
        }

        public RecepieListItemVm(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;

            Items = new List<RecepieListItemVm>();    
        }

        public void AddItem(int recepieId, string name)
        {
            Items.Add(new RecepieListItemVm(_eventAggregator)
            {
                RecepieId = recepieId,
                Name = name,
                Parent = this
            });
        }
    }
}
