﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models
{
    public class RecepieVm : WrappedModelVm<Recepie>
    {
        public RecepieVm(Recepie model) : base(model)
        {
            IngredientGroups = new BindableCollection<IngredientGroupVm>();            
            model.IngredientGroups.Apply(x => IngredientGroups.Add(new IngredientGroupVm(x)));
        }

        public string Name
        {
            get { return Model.Name; }
            set { SetValue<Recepie>(x => x.Name, value); }
        }

        public string Reference
        {
            get { return Model.Reference; }
            set { SetValue<Recepie>(x => x.Reference, value); }
        }

        public string Content
        {
            get { return Model.Content; }
            set { SetValue<Recepie>(x => x.Content, value); }
        }

        public string Serves
        {
            get { return Model.Serves; }
            set { SetValue<Recepie>(x => x.Serves, value); }
        }

        public BindableCollection<IngredientGroupVm> IngredientGroups { get; private set; }

        public IList<TagVm> Tags
        {
            get { return Model.Tags.Select(x => new TagVm(x)).OrderBy(x => x.Name).ToList(); }
        }

        public string TagsAsText
        {
            get { return string.Join(", ", Model.Tags.Select(x => x.Name).OrderBy(x => x)); }
            set { }
        }

        public void AddTag(TagVm tag)
        {
            AddTag(tag.Name, tag.TagId);
        }

        public void AddTag(string tagName, int tagId = 0)
        {
            if (Model.Tags.Any(x => x.Name == tagName))
            {
                return;
            }

            Model.Tags.Add(new Tag {Name = tagName, TagId = tagId});
            IsDirty = true;
            NotifyOfTagsChange();
        }

        public bool IsTagUnique(string tagName)
        {
            return Model.Tags.All(x => x.Name != tagName);
        }

        public void RemoveTag(TagVm tag)
        {
            var modelTag = Model.Tags.FirstOrDefault(x => x.Name == tag.Name);
            if (modelTag != null)
            {
                Model.Tags.Remove(modelTag);
                NotifyOfTagsChange();
                IsDirty = true;
            }
        }

        private void NotifyOfTagsChange()
        {
            NotifyOfPropertyChange(() => Tags);
            NotifyOfPropertyChange(() => TagsAsText);
        }

        public IngredientGroupVm AddIngredientGroup(string ingredientGroupName)
        {
            var ingredientGroupModel = new IngredientGroup
            {
                Name = ingredientGroupName
            };
            Model.IngredientGroups.Add(ingredientGroupModel);
            var ingredientGroupVm = new IngredientGroupVm(ingredientGroupModel);
            IngredientGroups.Add(ingredientGroupVm);
            NotifyOfPropertyChange(() => IngredientGroups);
            IsDirty = true;
            return ingredientGroupVm;
        }

        public void RemoveIngredientGroup(IngredientGroupVm selectedIngredientGroup)
        {
            var modelIngriedientsGroup = Model.IngredientGroups.First(x => x.Name == selectedIngredientGroup.Name);
            Model.IngredientGroups.Remove(modelIngriedientsGroup);

            IngredientGroups.Remove(selectedIngredientGroup);
            NotifyOfPropertyChange(() => IngredientGroups);
            IsDirty = true;
        }

        public bool Validate()
        {
            bool isValid = !string.IsNullOrWhiteSpace(Name);

            if (string.IsNullOrWhiteSpace(Content))
            {
                isValid = false;
            }

            if (!Tags.Any())
            {
                isValid = false;
            }

            return isValid;
        }
    }
}