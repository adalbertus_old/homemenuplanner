﻿namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models
{
    public enum QuestionDialogType
    {
        OKButtonOnly,
        OKCancelButton,
    }

    public class QuestionDialogVm
    {
        public string Title { get; set; }
        public string Message { get; set; }

        public QuestionDialogType DialogType { get; set; }
    }
}
