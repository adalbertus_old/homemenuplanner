﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models
{
    public class TagVm : WrappedModelVm<Tag>
    {
        public int TagId
        {
            get { return Model.TagId; } 
        }

        public string Name
        {
            get { return Model.Name; }
            set
            {
                Model.Name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public TagVm(Tag model) : base(model)
        {
        }
    }
}
