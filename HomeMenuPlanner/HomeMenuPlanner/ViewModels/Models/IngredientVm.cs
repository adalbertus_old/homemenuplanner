﻿using System.Text.RegularExpressions;
using Pietkiewicz.pl.HomeMenuPlanner.Common.Extensions;
using Pietkiewicz.pl.HomeMenuPlanner.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models
{
    public class IngredientVm : WrappedModelVm<Ingredient>
    {
        public IngredientVm(Ingredient model)
            : base(model)
        {
        }

        public string Name
        {
            get { return Model.Name; }
            set
            {
                Model.Name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        private string _value;
        public string Value
        {
            get { return string.Format("{0} {1}", Model.Value, Model.UnitKey); }
            set
            {
                if (TryParseAndSetValueAndUnitKey(string.Format("{0} {1}", value, Name)))
                {
                    NotifyOfPropertyChange(() => Value);
                }
            }
        }

        public static Ingredient Parse(string ingredienText)
        {
            // 1kg cukru
            // 0.5 łyżeczki soli
            // pektyna jabłkowa
            // value _SPACE_ unit _SPACE_ name
            // value _NO_SPACE_ unit _SPACE_ name
            // name
            //                     _______value__________  _unit_ name
            Regex r = new Regex(@"([0-9]*\.?\,?/?-?[0-9]*)(\s?\w+)(.*)");

            var matches = r.Match(ingredienText);
            if (matches.Groups.Count == 4)
            {
                if (string.IsNullOrWhiteSpace(matches.Groups[1].Value))
                {
                    return new Ingredient
                    {
                        Name = ingredienText,
                        Value = string.Empty,
                        UnitKey = string.Empty
                    };
                }

                var ingredient = new Ingredient
                {
                    Name = matches.Groups[3].Value.Trim(),
                    Value = matches.Groups[1].Value.Trim(),
                    UnitKey = matches.Groups[2].Value.Trim()
                };

                if (string.IsNullOrWhiteSpace(ingredient.Name))
                {
                    ingredient.Name = ingredient.UnitKey;
                    ingredient.UnitKey = string.Empty;
                }
                return ingredient;
            }
            return null;
        }

        private bool TryParseAndSetValueAndUnitKey(string ingredientText)
        {
            var ingriedientModel = Parse(ingredientText);
            if (ingriedientModel == null)
            {
                return false;
            }

            Model.UnitKey = ingriedientModel.UnitKey;
            Model.Value = ingriedientModel.Value;
            return true;
        }

    }
}