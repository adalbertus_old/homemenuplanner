﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Caliburn.Micro;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models
{
    public class WrappedModelVm<TModel> : PropertyChangedBase
    {
        public TModel Model { get; private set; }
        public bool IsDirty { get; set; }
        public WrappedModelVm(TModel model)
        {
            Model = model;
        }

        public void SetValue<T>(Expression<Func<T, object>> member, object value)
        {
            var memberSelectorExpression = member.Body as MemberExpression;
            if (memberSelectorExpression != null)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    property.SetValue(Model, value);
                    NotifyOfPropertyChange(property.Name);
                    IsDirty = true;
                }
            }
        }
    }
}
