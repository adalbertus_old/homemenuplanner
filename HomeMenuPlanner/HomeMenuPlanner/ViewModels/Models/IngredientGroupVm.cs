﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Common;
using Pietkiewicz.pl.HomeMenuPlanner.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models
{
    public class IngredientGroupVm : WrappedModelVm<IngredientGroup>
    {
        public IngredientGroupVm(IngredientGroup model) : base(model)
        {
            Ingredients = new BindableCollection<IngredientVm>();
            model.Ingredients.Apply(x => Ingredients.Add(new IngredientVm(x)));
        }

        public string Name
        {
            get { return Model.Name; }
            set { SetValue<IngredientGroup>(x => x.Name, value); }
        }

        private BindableCollection<IngredientVm> _ingredients;
        public BindableCollection<IngredientVm> Ingredients
        {
            get { return _ingredients; }
            set
            {
                _ingredients = value;               
                NotifyOfPropertyChange(() => Ingredients);
            }
        }

        private string _newIngredientName;

        public string NewIngredientName
        {
            get { return _newIngredientName; }
            set
            {
                _newIngredientName = value;
                NotifyOfPropertyChange(() => NewIngredientName);
            }
        }

        public bool CanAddNewIngredient
        {
            get { return !string.IsNullOrWhiteSpace(NewIngredientName); }
        
        }

        public bool AddNewIngredient()
        {
            if (!CanAddNewIngredient)
            {
                return false;
            }
            var ingredientModel = IngredientVm.Parse(NewIngredientName);
            if (ingredientModel == null)
            {
                return false;
            }

            Model.Ingredients.Add(ingredientModel);
            Ingredients.Add(new IngredientVm(ingredientModel));

            NewIngredientName = string.Empty;

            return true;
        }

        public void RemoveIngredient(IngredientVm ingredient)
        {
            Model.Ingredients.Remove(ingredient.Model);
            Ingredients.Remove(ingredient);
        }
    }
}
