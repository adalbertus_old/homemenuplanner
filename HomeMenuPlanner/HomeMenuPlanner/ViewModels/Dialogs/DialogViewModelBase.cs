﻿using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Services;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Dialogs
{
    public class DialogViewModelBase<TResult> : ViewModelBase
    {
        public TResult Result { get; set; }
        public bool IsOpen { get; set; }

        public DialogViewModelBase(IEventAggregator eventAggregator, IDatabaseService database, IDialogService dialogService)
            : base(eventAggregator, database, dialogService)
        {
            IsOpen = true;
        }

        public virtual void Finish()
        {
            IsOpen = false;
        }

        public virtual void Cancel()
        {
            IsOpen = false;
        }
    }
}
