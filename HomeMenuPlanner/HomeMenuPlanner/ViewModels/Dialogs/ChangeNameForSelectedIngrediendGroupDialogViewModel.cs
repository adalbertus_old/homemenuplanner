﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Services;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;
using Syncfusion.Windows.Tools.Controls;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Dialogs
{
    public class ChangeNameForSelectedIngrediendGroupDialogViewModel : DialogViewModelBase<bool>
    {
        public ChangeNameForSelectedIngrediendGroupDialogViewModel(IEventAggregator eventAggregator, IDatabaseService database, IDialogService dialogService) : base(eventAggregator, database, dialogService)
        {
        }

        public IngredientGroupVm IngredientGroup { get; private set; }        
        
        private string _oldName;
        public string OldName
        {
            get { return _oldName; }
            set
            {
                _oldName = value;
                NotifyOfPropertyChange(() => OldName);
            }
        }

        private string _newName;
        public string NewName
        {
            get { return _newName; }
            set
            {
                _newName = value;
                NotifyOfPropertyChange(() => NewName);
                NotifyOfPropertyChange(() => CanFinish);
            }
        }

        public bool CanFinish
        {
            get { return !string.IsNullOrWhiteSpace(NewName); }       
        }


        public override void Initialize(object parameter = null)
        {
            IngredientGroup = parameter as IngredientGroupVm;
            OldName = IngredientGroup.Name;
        }

        public override void Finish()
        {
            IngredientGroup.Name = NewName;
            base.Finish();
        }
    }
}
