﻿using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Services;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Dialogs
{
    public class QuestionDialogViewModel : DialogViewModelBase<bool>
    {
        public string Title
        {
            get
            {
                if (QuestionViewModel != null)
                {
                    return QuestionViewModel.DisplayName;
                }

                return DisplayName;
            }
        }

        private ViewModelBase _questionViewModel;

        public ViewModelBase QuestionViewModel
        {
            get { return _questionViewModel; }
            set
            {
                _questionViewModel = value;
                NotifyOfPropertyChange(() => QuestionViewModel);
            }
        }

        private string _question;

        public string Question
        {
            get { return _question; }
            set
            {
                _question = value;
                NotifyOfPropertyChange(() => Question);
            }
        }

        public QuestionDialogViewModel(IEventAggregator eventAggregator, IDatabaseService database, IDialogService dialogService) : base(eventAggregator, database, dialogService)
        {
        }

        public override void Initialize(object parameter = null)
        {
            if (parameter is ViewModelBase)
            {
                QuestionViewModel = parameter as ViewModelBase;
                Question = string.Empty;
            }
            if (parameter is QuestionDialogVm)
            {
                QuestionViewModel = null;
                var questionDialogVm = parameter as QuestionDialogVm;
                DisplayName = questionDialogVm.Title;
                Question = questionDialogVm.Message;
            }
        }

        public override void Finish()
        {
            Result = true;
            base.Finish();
        }

        public override void Cancel()
        {
            Result = false;
            base.Cancel();
        }
    }
}
