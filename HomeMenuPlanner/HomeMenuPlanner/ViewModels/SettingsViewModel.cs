﻿using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Common;
using Pietkiewicz.pl.HomeMenuPlanner.Models;
using Pietkiewicz.pl.HomeMenuPlanner.Services;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {
        private readonly ISettingsService _settingsService;

        public SettingsViewModel(IEventAggregator eventAggregator, IDatabaseService database, IDialogService dialogService, ISettingsService settingsService)
            : base(eventAggregator, database, dialogService)
        {
            _settingsService = settingsService;
        }

        #region Properties
        public bool CanChangeSettings
        {
            get { return false; }
        }

        #endregion

        public override void Initialize(object parameter = null)
        {
            NotifyOfPropertyChange(() => CanChangeSettings);
        }

        public void ChangeSettings()
        {
            if (CanChangeSettings)
            {
            }
            PublishSimpleCommand(CommandType.CloseSettingsFlyout);
        }

        public void CancelSettings()
        {
            PublishSimpleCommand(CommandType.CloseSettingsFlyout);
        }
    }
}
