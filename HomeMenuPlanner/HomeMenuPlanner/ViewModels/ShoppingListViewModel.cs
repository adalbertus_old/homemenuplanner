﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Services;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels
{
    public class ShoppingListViewModel : ViewModelBase
    {
        public ShoppingListViewModel(IEventAggregator eventAggregator, IDatabaseService database, IDialogService dialogService) : base(eventAggregator, database, dialogService)
        {
        }
    }
}
