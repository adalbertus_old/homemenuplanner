﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Common;
using Pietkiewicz.pl.HomeMenuPlanner.Models;
using Pietkiewicz.pl.HomeMenuPlanner.Services;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Dialogs;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels
{
    public class RecipiesListViewModel : ViewModelBase
    {
        public RecipiesListViewModel(IEventAggregator eventAggregator, IDatabaseService database, IDialogService dialogService)
            : base(eventAggregator, database, dialogService)
        {
            _recepieItems = new BindableCollection<RecepieListItemVm>();

            LoadData();
        }

        private string _searchPattern;
        public string SearchPattern
        {
            get { return _searchPattern; }
            set
            {
                _searchPattern = value;
                NotifyOfPropertyChange(() => SearchPattern);
                NotifyOfPropertyChange(() => RecepieItems);
            }
        }

        private readonly BindableCollection<RecepieListItemVm> _recepieItems;
        public IEnumerable<RecepieListItemVm> RecepieItems
        {
            get { return GetFilteredRecepieList(); }
        }

        protected override void HandleSimpleCommands(SimpleCommand command)
        {
            switch (command.CommandType)
            {
                case CommandType.CloseEditRecipie:
                    LoadData();
                    break;
            }
        }

        public void LoadData()
        {
            _recepieItems.Clear();
            var recepieList = Database.Query<RecepieHeader>("SELECT * FROM [RecepieList]").ToList();
            foreach (var recepieHeader in recepieList)
            {
                var recepie = RecepieItems.FirstOrDefault(x => x.Name == recepieHeader.TagName);
                if (recepie == null)
                {
                    recepie = new RecepieListItemVm(EventAggregator)
                    {
                        Name = recepieHeader.TagName
                    };
                    _recepieItems.Add(recepie);
                }

                if (recepieHeader.RecepieId > 0)
                {
                    recepie.AddItem(recepieHeader.RecepieId, recepieHeader.RecepieName);
                }
            }
        }

        public void AddRecipie()
        {
            PublishSimpleCommand(CommandType.OpenEditRecipie);
        }

        private IEnumerable<RecepieListItemVm> GetFilteredRecepieList()
        {
            _recepieItems.IsNotifying = false;
            _recepieItems.Apply(x =>
            {
                x.Items.Apply(y => y.IsSelected = false);
                x.IsSelected = false;
            });
            _recepieItems.IsNotifying = true;

            if (string.IsNullOrWhiteSpace(SearchPattern))
            {
                return _recepieItems;
            }

            var recepies = _recepieItems.SelectMany(x => x.Items).Where(x => x.Name.ToLowerInvariant().Contains(SearchPattern.ToLowerInvariant())).ToList();
            var tags = _recepieItems.Where(x => x.Name.ToLowerInvariant().Contains(SearchPattern.ToLowerInvariant())).ToList();

            // add all tags for recepies found
            foreach (var recepie in recepies)
            {
                var tag = tags.FirstOrDefault(x => x.Name == recepie.Parent.Name);
                if (tag == null)
                {
                    tag = new RecepieListItemVm(EventAggregator)
                    {
                        Name = recepie.Parent.Name
                    };
                    tag.AddItem(recepie.RecepieId, recepie.Name);
                    tags.Add(tag);
                }
            }
            return tags.OrderBy(x => x.Name);
        }
    }
}
