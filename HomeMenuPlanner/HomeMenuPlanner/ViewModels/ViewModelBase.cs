﻿using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Common;
using Pietkiewicz.pl.HomeMenuPlanner.Services;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels
{
    public class ViewModelBase : Screen, IHandle<object>
    {
        protected readonly IEventAggregator EventAggregator;
        protected readonly IDialogService DialogService;
        protected readonly IDatabaseService Database;

        public ViewModelBase(IEventAggregator eventAggregator, IDatabaseService database, IDialogService dialogService)
        {
            EventAggregator = eventAggregator;
            Database = database;
            DialogService = dialogService;
            EventAggregator.Subscribe(this);
        }

        public virtual void Initialize(object parameter = null)
        {
            
        }

        public void PublishSimpleCommand(CommandType commandType, object commandData = null)
        {
            EventAggregator.PublishOnUIThread(new SimpleCommand
            {
                CommandType = commandType,
                Data = commandData
            });
        }

        public virtual void Handle(object message)
        {
            if (message == null)
            {
                return;
            }

            if (message is SimpleCommand)
            {
                HandleSimpleCommands((SimpleCommand) message);
            }
        }

        protected virtual void HandleSimpleCommands(SimpleCommand command)
        {
        }
    }
}
