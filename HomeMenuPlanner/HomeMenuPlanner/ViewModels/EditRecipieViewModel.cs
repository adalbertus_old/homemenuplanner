﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Common;
using Pietkiewicz.pl.HomeMenuPlanner.Models;
using Pietkiewicz.pl.HomeMenuPlanner.Services;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Dialogs;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels
{
    public class EditRecipieViewModel : ViewModelBase
    {
        private readonly IRecepieService _recepieService;

        public EditRecipieViewModel(IEventAggregator eventAggregator, IDatabaseService database, IDialogService dialogService, IRecepieService recepieService)
            : base(eventAggregator, database, dialogService)
        {
            _recepieService = recepieService;
            AllTags = new BindableCollection<TagVm>();
        }

        #region Properties and fields
        private RecepieVm _recepie;
        public RecepieVm Recepie
        {
            get { return _recepie; }
            set
            {
                _recepie = value;
                NotifyOfPropertyChange(() => RecepieName);
                NotifyOfPropertyChange(() => CanSave);
            }
        }

        public string RecepieName
        {
            get { return _recepie.Name; }
            set
            {
                _recepie.Name = value;
                NotifyOfPropertyChange(() => RecepieName);
                NotifyOfPropertyChange(() => CanSave);
            }
        }

        public string RecepieContent
        {
            get { return _recepie.Content; }
            set
            {
                _recepie.Content = value;
                NotifyOfPropertyChange(() => RecepieContent);
                NotifyOfPropertyChange(() => CanSave);
            }
        }

        public IList<TagVm> RecepieTags
        {
            get { return Recepie.Tags; }
        }

        public BindableCollection<IngredientGroupVm> RecepieIngredientGroups
        {
            get { return Recepie.IngredientGroups; }
        }

        public bool CanSave
        {
            get { return CheckIfRecepieCanBeSaved(); }
        }

        private BindableCollection<TagVm> _allTags;

        public BindableCollection<TagVm> AllTags
        {
            get { return _allTags; }
            set
            {
                _allTags = value;
                NotifyOfPropertyChange(() => AllTags);
            }
        }

        private string _tagName;

        public string TagName
        {
            get { return _tagName; }
            set
            {
                _tagName = value;
                NotifyOfPropertyChange(() => TagName);
                NotifyOfPropertyChange(() => CanAddTag);
            }
        }

        private TagVm _selectedTag;

        public TagVm SelectedTag
        {
            get { return _selectedTag; }
            set
            {
                _selectedTag = value;
                NotifyOfPropertyChange(() => SelectedTag);
            }
        }

        private IngredientGroupVm _selectedIngredientGroup;

        public IngredientGroupVm SelectedIngredientGroup
        {
            get { return _selectedIngredientGroup; }
            set
            {
                _selectedIngredientGroup = value;
                if (_selectedIngredientGroup != null)
                {
                    _selectedIngredientGroup.PropertyChanged += (sender, args) =>
                    {
                        if (args.PropertyName == "NewIngredientName")
                        {
                            NotifyOfPropertyChange(() => CanAddNewIngredient);
                        }
                    };
                }
                NotifyOfPropertyChange(() => SelectedIngredientGroup);
            }
        }

        private string _ingredientGroupName;

        public string IngredientGroupName
        {
            get { return _ingredientGroupName; }
            set
            {
                _ingredientGroupName = value;
                NotifyOfPropertyChange(() => IngredientGroupName);
                NotifyOfPropertyChange(() => CanAddIngredientGroup);
                NotifyOfPropertyChange(() => CanAddNewIngredient);
            }
        }

        public bool CanAddTag
        {
            get { return !string.IsNullOrWhiteSpace(TagName); }
        }

        public bool CanAddIngredientGroup
        {
            get { return !string.IsNullOrWhiteSpace(IngredientGroupName); }
        }

        public bool CanAddNewIngredient
        {
            get
            {
                if (SelectedIngredientGroup == null)
                {
                    return false;
                }
                return SelectedIngredientGroup.CanAddNewIngredient;
            }
        }

        #endregion

        public void AddTag()
        {
            if (!CanAddTag)
            {
                return;
            }

            if (Recepie.IsTagUnique(TagName))
            {
                if (SelectedTag == null)
                {
                    Recepie.AddTag(TagName);
                }
                else
                {
                    Recepie.AddTag(SelectedTag);
                }
                NotifyOfPropertyChange(() => RecepieTags);
                NotifyOfPropertyChange(() => CanSave);
            }
            TagName = string.Empty;
            SelectedTag = null;
        }

        public void AddIngredientGroup()
        {
            if (!CanAddIngredientGroup)
            {
                return;
            }

            var newIngredientGroup = Recepie.AddIngredientGroup(IngredientGroupName);
            IngredientGroupName = string.Empty;
            SelectedIngredientGroup = newIngredientGroup;
        }

        public void DeleteIngredient(IngredientVm ingredient)
        {
            if (SelectedIngredientGroup == null)
            {
                return;
            }
            SelectedIngredientGroup.RemoveIngredient(ingredient);
        }

        public void DeleteTag(TagVm tag)
        {
            Recepie.RemoveTag(tag);
            NotifyOfPropertyChange(() => RecepieTags);
            NotifyOfPropertyChange(() => CanSave);
        }

        public void AddNewIngredient(IngredientGroupVm ingredientGroupVm)
        {
            ingredientGroupVm.AddNewIngredient();
        }

        public override void Initialize(object parameter = null)
        {
            LoadAllTags();

            int? recepieId = null;
            if (parameter != null)
            {
                recepieId = (int?)parameter;
            }

            LoadOrCreateRecepie(recepieId);

            if (!RecepieIngredientGroups.Any())
            {
                IngredientGroupName = "Składniki";
                AddIngredientGroup();
            }
            else
            {
                SelectedIngredientGroup = RecepieIngredientGroups.First();
            }
            Refresh();

        }

        private void LoadOrCreateRecepie(int? recepieId)
        {
            if (recepieId.HasValue)
            {
                Recepie = _recepieService.GetRecepie(recepieId.Value);
            }
            else
            {
                Recepie = new RecepieVm(new Recepie());
            }

        }

        private void LoadAllTags()
        {
            var allTags = Database.Query<Tag>("SELECT * FROM Tag t ORDER BY t.Name").ToList();

            AllTags.Clear();

            allTags.Apply(x => AllTags.Add(new TagVm(x)));
        }

        public async void RemoveSelectedIngredientGroup()
        {
            if (SelectedIngredientGroup == null)
            {
                return;
            }

            var dialogTitle = string.Format("Usuwanie grupy składników {0}", SelectedIngredientGroup.Name);
            if (RecepieIngredientGroups.Count() == 1)
            {
                DialogService.ShowMessageBox(dialogTitle,
                    "Nie można usunąć grupy składników. To jest jedyna zdefiniowana grupa.",
                    QuestionDialogType.OKButtonOnly);
                return;
            }

            var removeIngredientGroup = await DialogService.ShowMessageBox(dialogTitle, "Na pewno usunąć grupę składników?",
                QuestionDialogType.OKCancelButton);
            if (removeIngredientGroup)
            {
                Recepie.RemoveIngredientGroup(SelectedIngredientGroup);
                SelectedIngredientGroup = RecepieIngredientGroups.First();
            }
        }

        public async void ChangeNameSelectedIngredientGroup()
        {
            if (SelectedIngredientGroup == null)
            {
                return;
            }

            var result = await DialogService.ShowDialogAsync<ChangeNameForSelectedIngrediendGroupDialogViewModel, bool>(SelectedIngredientGroup);
        }

        protected override void HandleSimpleCommands(SimpleCommand command)
        {
            switch (command.CommandType)
            {
                case CommandType.NewIngredientNameChanged:
                    NotifyOfPropertyChange(() => CanAddNewIngredient);
                    break;
            }
        }

        public void Cancel()
        {
            PublishSimpleCommand(CommandType.CloseEditRecipie);
        }

        private bool CheckIfRecepieCanBeSaved()
        {
            return Recepie.Validate();            
        }

        public void Save()
        {
            if (CanSave)
            {
                _recepieService.Save(Recepie);                
            }
            PublishSimpleCommand(CommandType.CloseEditRecipie);
        }
    }
}
