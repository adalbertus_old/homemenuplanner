﻿using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Common;
using Pietkiewicz.pl.HomeMenuPlanner.Services;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels
{
    public class RecipieDetailsViewModel : ViewModelBase
    {
        private readonly IRecepieService _recepieService;

        public RecipieDetailsViewModel(IEventAggregator eventAggregator, IDatabaseService database,
            IDialogService dialogService, IRecepieService recepieService)
            : base(eventAggregator, database, dialogService)
        {
            _recepieService = recepieService;
        }

        public RecepieVm Recepie { get; set; }


        public void EditRecipie()
        {
            PublishSimpleCommand(CommandType.OpenEditRecipie, Recepie.Model.RecepieId);
        }

        protected override void HandleSimpleCommands(SimpleCommand command)
        {
            switch (command.CommandType)
            {
                case CommandType.RecepieSelected:
                    var recepie = command.Data as RecepieListItemVm;
                    if (recepie != null && (recepie.IsSelected && recepie.RecepieId > 0))
                    {
                        LoadRecepie(recepie.RecepieId);
                    }
                    break;
            }
        }

        private void LoadRecepie(int recepieId)
        {
            Recepie = _recepieService.GetRecepie(recepieId);

            NotifyOfPropertyChange(() => Recepie);
        }
    }
}