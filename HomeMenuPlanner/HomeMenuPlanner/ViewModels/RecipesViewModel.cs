﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Services;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels
{
    public class RecipesViewModel : ViewModelBase
    {
        public RecipesViewModel(IEventAggregator eventAggregator, IDatabaseService database, IDialogService dialogService) : base(eventAggregator, database, dialogService)
        {
        }


        private RecipiesListViewModel _recipiesListViewModel;
        public RecipiesListViewModel RecipiesListViewModel
        {
            get { return _recipiesListViewModel; }
            set
            {
                _recipiesListViewModel = value;
                NotifyOfPropertyChange(() => RecipiesListViewModel);
            }
        }

        private RecipieDetailsViewModel _recipieDetailsViewModel;
        public RecipieDetailsViewModel RecipieDetailsViewModel
        {
            get { return _recipieDetailsViewModel; }
            set
            {
                _recipieDetailsViewModel = value;
                NotifyOfPropertyChange(() => RecipieDetailsViewModel);
            }
        }

    }
}
