using System;
using System.CodeDom;
using Caliburn.Micro;
using Pietkiewicz.pl.HomeMenuPlanner.Common;
using Pietkiewicz.pl.HomeMenuPlanner.Models;
using Pietkiewicz.pl.HomeMenuPlanner.Services;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Dialogs;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.ViewModels
{
    public class ShellViewModel : ViewModelBase, IShell
    {
        private readonly IDialogService _dialogService;
        private readonly ISettingsService _settingsService;

        #region Properties

        #region ViewModels
        private SettingsViewModel _settingsViewModel;
        public SettingsViewModel SettingsViewModel
        {
            get { return _settingsViewModel; }
            set
            {
                _settingsViewModel = value;
                NotifyOfPropertyChange(() => SettingsViewModel);
            }
        }

        private EditRecipieViewModel _editRecipieViewModel;
        public EditRecipieViewModel EditRecipieViewModel
        {
            get { return _editRecipieViewModel; }
            set
            {
                _editRecipieViewModel = value;
                NotifyOfPropertyChange(() => EditRecipieViewModel);
            }
        }


        private RecipesViewModel _recipesViewModel;
        public RecipesViewModel RecipesViewModel
        {
            get { return _recipesViewModel; }
            set
            {
                _recipesViewModel = value;
                NotifyOfPropertyChange(() => RecipesViewModel);
            }
        }

        private WeekPlanViewModel _weekPlanViewModel;
        public WeekPlanViewModel WeekPlanViewModel
        {
            get { return _weekPlanViewModel; }
            set
            {
                _weekPlanViewModel = value;
                NotifyOfPropertyChange(() => WeekPlanViewModel);
            }
        }

        private ShoppingListViewModel _shoppingListViewModel;
        public ShoppingListViewModel ShoppingListViewModel
        {
            get { return _shoppingListViewModel; }
            set
            {
                _shoppingListViewModel = value;
                NotifyOfPropertyChange(() => ShoppingListViewModel);
            }
        }


        #endregion

        private bool _isSettingsOpen;
        public bool IsSettingsOpen
        {
            get { return _isSettingsOpen; }
            set
            {
                _isSettingsOpen = value;
                NotifyOfPropertyChange(() => IsSettingsOpen);
            }
        }

        private bool _isEditRecepieOpen;
        public bool IsEditRecepieOpen
        {
            get { return _isEditRecepieOpen; }
            set
            {
                _isEditRecepieOpen = value;
                NotifyOfPropertyChange(() => IsEditRecepieOpen);
            }
        }


        #endregion

        public ShellViewModel(IEventAggregator eventAggregator, IDatabaseService database, ISettingsService settingsService, IDialogService dialogService)
            : base(eventAggregator, database, dialogService)
        {
            _settingsService = settingsService;

            DisplayName = "Domowe Menu";
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            InitializeDatabaseIfNeeded();

            LoadData();
        }

        private void InitializeDatabaseIfNeeded()
        {
            //Database.CreateShemaIfNeeded();
        }

        private void LoadData()
        {
        }

        #region Button commands
        public async void OpenSettings()
        {
            this.SettingsViewModel.Initialize();
            IsSettingsOpen = true;

        }
        #endregion

        protected override void HandleSimpleCommands(SimpleCommand command)
        {
            switch (command.CommandType)
            {
                case CommandType.CloseSettingsFlyout:
                    IsSettingsOpen = false;
                    break;
                case CommandType.OpenEditRecipie:
                    OpenEditRecepie((int?)command.Data);
                    break;
                case CommandType.CloseEditRecipie:
                    IsEditRecepieOpen = false;
                    break;
            }
        }

        private void OpenEditRecepie(int? recepieId)
        {
            EditRecipieViewModel.Initialize(recepieId);
            IsEditRecepieOpen = true;
        }
    }


}