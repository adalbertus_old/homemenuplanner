CREATE TABLE [Recepie] (
  [RecepieId] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
  [Name] VARCHAR(500), 
  [Reference] VARCHAR(1000), 
  [Serves] VARCHAR(250), 
  [Content] TEXT, 
  [Created] DATETIME NOT NULL, 
  [Modified] DATETIME NOT NULL, 
  [Tips] TEXT);


CREATE TABLE [IngredientGroup] (
  [IngredientGroupId] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
  [RecepieId] INTEGER NOT NULL REFERENCES [Recepie]([RecepieId]) ON DELETE CASCADE, 
  [Name] VARCHAR(500));


CREATE TABLE [Ingredient] (
  [IngredientId] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
  [IngredientGroupId] INTEGER NOT NULL REFERENCES [IngredientGroup]([IngredientGroupId]) ON DELETE CASCADE, 
  [Name] VARCHAR(500), 
  [Value] VARCHAR(500), 
  [UnitKey] VARCHAR(16));

CREATE INDEX [IngredientGroupIdIndex] ON [Ingredient] ([IngredientGroupId]);


CREATE TABLE [Tag]
(
	[TagId] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,       
	[Name] VARCHAR(500)      
);


CREATE TABLE [RecepieTag] (
  [RecepieId] INTEGER NOT NULL REFERENCES [Recepie]([RecepieId]) ON DELETE CASCADE, 
  [TagId] INTEGER NOT NULL REFERENCES [Tag]([TagId]), 
  UNIQUE([RecepieId], [TagId]));

CREATE INDEX [TagIdIndex] ON [RecepieTag] ([TagId]);


CREATE TABLE [Setting] (
	[Key] VARCHAR(64) PRIMARY KEY, 
	[IsActive] INTEGER(1) DEFAULT 1,
	[Value] TEXT,
	[Decription] TEXT
);


CREATE VIEW [RecepieList] AS
select t.[Name] TagName, r.[Name] RecepieName, r.[RecepieId] from Tag t
left join [RecepieTag] rt on (rt.[TagId] = t.[TagId])
left join [Recepie] r on (r.[RecepieId] = rt.[RecepieId])
order by t.[Name], r.[Name];
