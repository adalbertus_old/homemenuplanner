﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pietkiewicz.pl.HomeMenuPlanner.Models
{
    [PetaPoco.TableName("Ingredient")]
    [PetaPoco.PrimaryKey("IngredientId")]
    public class Ingredient
    {
        public int IngredientId { get; set; }
        public int IngredientGroupId { get; set; }

        public string Name { get; set; }
        public string Value { get; set; }
        public string UnitKey { get; set; }

        public override string ToString()
        {
            return string.Format("{0} ({1} {2})", Name, Value, UnitKey);
        }
    }
}
