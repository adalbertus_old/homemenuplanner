﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pietkiewicz.pl.HomeMenuPlanner.Models
{
    [PetaPoco.TableName("IngredientGroup")]
    [PetaPoco.PrimaryKey("IngredientGroupId")]
    public class IngredientGroup
    {
        public int IngredientGroupId { get; set; }
        public int RecepieId { get; set; }
        public string Name { get; set; }

        [PetaPoco.Ignore]
        public IList<Ingredient> Ingredients { get; set; }

        public IngredientGroup()
        {
            Ingredients = new List<Ingredient>();
        }
    }
}
