﻿namespace Pietkiewicz.pl.HomeMenuPlanner.Models
{
    [PetaPoco.TableName("Setting")]
    [PetaPoco.PrimaryKey("Key", AutoIncrement = false)]
    public class Setting
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public bool IsActive { get; set; }
        public string Decription { get; set; }

        public static class Keys
        {
            public static string BudgetStartDate = "BudgetStartDate";
        }
    }
}
