﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pietkiewicz.pl.HomeMenuPlanner.Models
{
    [PetaPoco.TableName("Recepie")]
    [PetaPoco.PrimaryKey("RecepieId")]
    public class Recepie
    {
        public int RecepieId { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public string Content { get; set; }
        public string Serves { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [PetaPoco.Ignore]
        public IList<IngredientGroup> IngredientGroups { get; set; }

        [PetaPoco.Ignore]
        public IList<Tag> Tags { get; set; }

        public Recepie()
        {
            IngredientGroups = new List<IngredientGroup>();
            Tags = new List<Tag>();
            Created = DateTime.Now;
            Modified = Created;
        }
    }
}
