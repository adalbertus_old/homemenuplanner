﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pietkiewicz.pl.HomeMenuPlanner.Models
{
    [PetaPoco.TableName("Tag")]
    [PetaPoco.PrimaryKey("TagId")]
    public class Tag
    {
        public int TagId { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format("{0} ({1})", Name, TagId);
        }
    }
}
