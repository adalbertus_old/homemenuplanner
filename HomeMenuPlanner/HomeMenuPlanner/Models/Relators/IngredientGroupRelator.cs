﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pietkiewicz.pl.HomeMenuPlanner.Models.Relators
{
    internal class IngredientGroupRelator
    {
        private IngredientGroup _currentIngredientGroup;

        public IngredientGroup MapIt(IngredientGroup ingredientGroup, Ingredient ingredient)
        {
            if (ingredientGroup == null)
            {
                return _currentIngredientGroup;
            }

            if (_currentIngredientGroup != null && _currentIngredientGroup.IngredientGroupId == ingredientGroup.IngredientGroupId)
            {
                _currentIngredientGroup.Ingredients.Add(ingredient);

                return null;
            }

            var prev = _currentIngredientGroup;
            _currentIngredientGroup = ingredientGroup;
            _currentIngredientGroup.Ingredients = new List<Ingredient>();
            _currentIngredientGroup.Ingredients.Add(ingredient);

            return prev;
        }
    }
}
