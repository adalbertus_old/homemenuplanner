﻿using System;
using Pietkiewicz.pl.HomeMenuPlanner.Common;
using Pietkiewicz.pl.HomeMenuPlanner.Common.Extensions;
using Pietkiewicz.pl.HomeMenuPlanner.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.Services
{
    public class SettingsService : ServiceBase, ISettingsService
    {
        public SettingsService(IDatabaseService databaseService) : base(databaseService)
        {
        }

        public int GetInt(string key, int defaultValue = 0)
        {
            key.ThrowIfNull("key");
            var setting = DatabaseService.FirstOrDefault<Setting>("WHERE Key = @0", key);

            if (setting == null)
            {
                Save(key, defaultValue);
                return defaultValue;
            }

            int value;
            if (!int.TryParse(setting.Value, out value))
            {
                return defaultValue;
            }
            return value;
        }

        public string GetString(string key, string defaultValue = "")
        {
            key.ThrowIfNull("key");
            var setting = DatabaseService.FirstOrDefault<Setting>("WHERE Key = @0", key);

            if (setting == null)
            {
                Save(key, defaultValue);
                return defaultValue;
            }
            return setting.Value;
        }

        public void Save<T>(string key, T value)
        {
            key.ThrowIfNull("key");

            var setting = DatabaseService.FirstOrDefault<Setting>("WHERE Key = @0", key);
            if (setting == null)
            {
                setting = new Setting
                {
                    IsActive = true,
                    Key = key,
                    Value = ValueToString(value)
                };

                DatabaseService.Insert(setting);
            }
            else
            {
                setting.Value = ValueToString(value);
                DatabaseService.Update(setting);
            }
        }

        private string ValueToString<T>(T value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            string result = string.Empty;

            TypeSwitch.Do(value,
                TypeSwitch.Case<DateTime>(x => result = x.ToString("s")),
                TypeSwitch.Default(() => result = value.ToString()));

            return result;
        }
    }
}