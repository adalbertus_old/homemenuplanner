﻿using System.Collections.Generic;
using Pietkiewicz.pl.HomeMenuPlanner.Database;

namespace Pietkiewicz.pl.HomeMenuPlanner.Services
{
    public interface IDatabaseService : IDatabase
    {
        void CreateShemaIfNeeded();
        void SaveAll<TModel>(IEnumerable<TModel> items);
        IEnumerable<T> Query<T>();
        int Count<T>();
        int Count(string tableName);
        int Count<T>(string sql, params object[] args);
        int Count(string tableName, string sql, params object[] args);
        T FirstOrDefault<T>();
    }
}