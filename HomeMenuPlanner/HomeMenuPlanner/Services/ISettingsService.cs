﻿namespace Pietkiewicz.pl.HomeMenuPlanner.Services
{
    public interface ISettingsService
    {
        int GetInt(string key, int defaultValue = 0);
        string GetString(string key, string defaultValue = "");
        void Save<T>(string key, T value);
    }
}
