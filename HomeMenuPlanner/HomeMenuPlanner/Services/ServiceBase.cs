﻿namespace Pietkiewicz.pl.HomeMenuPlanner.Services
{
    public class ServiceBase
    {
        protected readonly IDatabaseService DatabaseService;

        public ServiceBase(IDatabaseService databaseService)
        {
            DatabaseService = databaseService;
        }
    }
}