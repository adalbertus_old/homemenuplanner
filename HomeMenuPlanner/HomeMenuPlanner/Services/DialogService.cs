﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Caliburn.Micro;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Dialogs;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.Services
{
    public class DialogService : IDialogService
    {
        private readonly List<ContentControl> _ContentControls = new List<ContentControl>();

        public static MetroWindow MainWindow
        {
            get { return ((MetroWindow) Application.Current.MainWindow); }
        }

        public async Task<bool> ShowMessageBox(string title, string message, QuestionDialogType dialogType)
        {

            var result = await ShowDialogAsync<QuestionDialogViewModel, bool>(new QuestionDialogVm
            {
                Title = title,
                Message = message,
                DialogType = dialogType
            });

            return result;
        }

        public async Task<TResult> ShowDialogAsync<TDialogViewModel, TResult>(object parameter = null)
            where TDialogViewModel : DialogViewModelBase<TResult>
        {
            var viewModelWithView = CreateAndBindViewModelWithView<TDialogViewModel>(parameter);
            var viewModel = viewModelWithView.Item1;
            var viewInstance = viewModelWithView.Item2;

            HidePreviousDialogShowCurrent(viewInstance);

            await Task.Run(() =>
            {
                do
                {
                    Thread.Sleep(100);
                } while (viewModel.IsOpen);
            });

            HideCurrentDialogShowPrevious(viewInstance);

            return viewModel.Result;
        }

        public async Task<bool> ShowQuestionDialogAsync<TDialogViewModel>(object parameter = null) where TDialogViewModel : DialogViewModelBase<bool>
        {
            var viewModelWithView = CreateAndBindViewModelWithView<TDialogViewModel>(parameter);
            var viewModel = viewModelWithView.Item1;

            return await ShowDialogAsync<QuestionDialogViewModel, bool>(viewModel);
        }

        private async void HideCurrentDialogShowPrevious(ContentControl viewInstance)
        {
            _ContentControls.Remove(viewInstance);

            await MainWindow.HideMetroDialogAsync(viewInstance as BaseMetroDialog);

            var previousDialog = _ContentControls.LastOrDefault();
            if (previousDialog != null)
            {
                await MainWindow.ShowMetroDialogAsync(previousDialog as BaseMetroDialog, new MetroDialogSettings
                {
                    ColorScheme = MetroDialogColorScheme.Accented
                });
            }
        }

        private async void HidePreviousDialogShowCurrent(ContentControl viewInstance)
        {
            var previousDialog = _ContentControls.LastOrDefault();
            if (previousDialog != null)
            {
                await MainWindow.HideMetroDialogAsync(previousDialog as BaseMetroDialog);
            }

            _ContentControls.Add(viewInstance);

            await MainWindow.ShowMetroDialogAsync(viewInstance as BaseMetroDialog, new MetroDialogSettings
            {
                ColorScheme = MetroDialogColorScheme.Accented
            });
        }

        private static string GetViewFullName<TDialogViewModel>() where TDialogViewModel : ViewModelBase
        {
            var viewFullName = typeof(TDialogViewModel).FullName;
            viewFullName = viewFullName.Replace("ViewModels", "Views");
            viewFullName = viewFullName.Replace("ViewModel", "View");
            return viewFullName;
        }

        private static Tuple<TDialogViewModel, ContentControl> CreateAndBindViewModelWithView<TDialogViewModel>(object parameter) where TDialogViewModel : ViewModelBase
        {
            var viewFullName = GetViewFullName<TDialogViewModel>();

            var viewModel = IoC.Get<TDialogViewModel>();
            if (viewModel == null)
            {
                throw new NullReferenceException(string.Format("Unable to create ViewModel for view: {0}", viewFullName));
            }

            var viewInstance = IoC.GetInstance(Type.GetType(viewFullName), null) as ContentControl;
            if (viewInstance == null)
            {
                throw new NullReferenceException(string.Format("Unable to create instance of view: {0}", viewFullName));
            }

            ViewModelBinder.Bind(viewModel, viewInstance, null);

            viewModel.Initialize(parameter);
            return new Tuple<TDialogViewModel, ContentControl>(viewModel, viewInstance);
        }
    }
}