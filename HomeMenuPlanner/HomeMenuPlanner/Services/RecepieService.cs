﻿using System;
using System.Linq;
using Castle.Core.Internal;
using PetaPoco;
using Pietkiewicz.pl.HomeMenuPlanner.Models;
using Pietkiewicz.pl.HomeMenuPlanner.Models.Relators;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.Services
{
    public class RecepieService : IRecepieService
    {
        private readonly IDatabaseService _databaseService;

        public RecepieService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }


        public RecepieVm GetRecepie(int recepieId)
        {
            var recepie = _databaseService.SingleOrDefault<Recepie>("WHERE RecepieId = @0", recepieId);
            recepie.Tags = _databaseService.Query<Tag>("SELECT t.* FROM [Tag] t " +
                                           "JOIN [RecepieTag] rt ON (rt.[TagId] = t.[TagId]) " +
                                           "WHERE rt.[RecepieId] = @0", recepieId).ToList();
            recepie.IngredientGroups = _databaseService.Fetch<IngredientGroup, Ingredient, IngredientGroup>(new IngredientGroupRelator().MapIt,
                "SELECT * FROM [IngredientGroup] " +
                "LEFT JOIN [Ingredient] ON ([Ingredient].[IngredientGroupId] = [IngredientGroup].[IngredientGroupId]) " +
                "WHERE [IngredientGroup].[RecepieId] = @0 " +
                "ORDER BY [IngredientGroup].[Name], [Ingredient].[Name] ", recepieId).ToList();

            var recepieVm = new RecepieVm(recepie);
            return recepieVm;
        }

        public void Save(RecepieVm recepie)
        {
            using (var tx = _databaseService.GetTransaction())
            {
                recepie.Model.Modified = DateTime.Now;

                _databaseService.Save(recepie.Model);
                // Save all not saved tags...
                _databaseService.SaveAll(recepie.Model.Tags.Where(x => _databaseService.IsNew(x)));

                // delete all relations between recepie <-> tag, then create it
                _databaseService.Execute("DELETE FROM [RecepieTag] WHERE [RecepieId] = @0", recepie.Model.RecepieId);

                // insert all tags
                foreach (var tag in recepie.Model.Tags)
                {
                    _databaseService.Execute("INSERT INTO [RecepieTag] VALUES (@0, @1)", recepie.Model.RecepieId, tag.TagId);
                }

                // delete all relations between ingredients and ingredient groups and recepie
                _databaseService.Execute("DELETE FROM [IngredientGroup] WHERE [RecepieId] = @0", recepie.Model.RecepieId);

                // insert all ingredient group
                recepie.Model.IngredientGroups.ForEach(x =>
                {
                    x.IngredientGroupId = 0;
                    x.RecepieId = recepie.Model.RecepieId;
                });
                _databaseService.SaveAll(recepie.Model.IngredientGroups);

                // insert all ingredients in specific group
                foreach (var ingredientGroup in recepie.Model.IngredientGroups)
                {
                    ingredientGroup.Ingredients.ForEach(x =>
                    {
                        x.IngredientId = 0;
                        x.IngredientGroupId = ingredientGroup.IngredientGroupId;
                    });
                    _databaseService.SaveAll(ingredientGroup.Ingredients);

                }

                tx.Complete();
            }

        }
    }
}