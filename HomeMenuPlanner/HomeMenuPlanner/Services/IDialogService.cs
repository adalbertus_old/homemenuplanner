using System.Threading.Tasks;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Dialogs;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.Services
{
    public interface IDialogService
    {
        Task<bool> ShowMessageBox(string title, string message, QuestionDialogType dialogType);      
        Task<TResult> ShowDialogAsync<TDialogViewModel, TResult>(object parameter = null) where TDialogViewModel : DialogViewModelBase<TResult>;
        Task<bool> ShowQuestionDialogAsync<TDialogViewModel>(object parameter = null) where TDialogViewModel : DialogViewModelBase<bool>;
    }
}