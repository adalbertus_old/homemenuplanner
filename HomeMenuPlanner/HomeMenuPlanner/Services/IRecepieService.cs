﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pietkiewicz.pl.HomeMenuPlanner.ViewModels.Models;

namespace Pietkiewicz.pl.HomeMenuPlanner.Services
{
    public interface IRecepieService
    {
        RecepieVm GetRecepie(int recepieId);
        void Save(RecepieVm recepie);
    }
}
